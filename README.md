# Miguel's dotfiles

## Ubuntu installation notes

### Dash to Panel

Install Gnome Extension Manager:

```bash
sudo apt install gnome-shell-extension-manager gnome-tweaks
```

Then add "Dash to Panel" to replace "Dock", so that all monitors get the same
taskbar.

### Git

```bash
git config --global --edit
git config --global core.editor vi
git config --global gpg.format ssh
git config --global user.signingkey ~/.ssh/id_rsa.pub
git config --global commit.gpgsign true
```

## Setup

```bash
sudo apt update -y
sudo apt install -y git zsh tmux neovim universal-ctags
git clone https://github.com/miguelgrinberg/dotfiles
cd dotfiles
./install
chsh -s /bin/zsh
sudo update-alternatives --install /usr/bin/vi vi /snap/bin/nvim 110
sudo update-alternatives --install /usr/bin/vim vim /snap/bin/nvim 110
```

## fzf

```bash
git clone --depth 2 https://github.com/junegunn/fzf.git ~/.fzf
~/.fzf/install
```

## GNOME Terminal

Edit profile in preferences:

- 160x50 characters
- Select `White on black` theme

Also install `MesloLGS NF` fonts from the [powerlevel 10k site](https://github.com/romkatv/powerlevel10k#manual-font-installation).

## Kitty Terminal (Ubuntu)

```
# install kitty locally
curl -L https://sw.kovidgoyal.net/kitty/installer.sh | sh /dev/stdin

# add to PATH through ~/bin links
ln -sr ~/.local/kitty.app/bin/kitty ~/.local/kitty.app/bin/kitten ~/bin

# install desktop file
cp ~/.local/kitty.app/share/applications/kitty.desktop ~/.local/share/applications/

# update paths in desktop file
sed -i "s|Icon=kitty|Icon=/home/$USER/.local/kitty.app/share/icons/hicolor/256x256/apps/kitty.png|g" ~/.local/share/applications/kitty*.desktop
sed -i "s|Exec=kitty|Exec=/home/$USER/.local/kitty.app/bin/kitty|g" ~/.local/share/applications/kitty*.desktop
```

## Docker install (Ubuntu)

```bash
sudo apt-get update
sudo apt-get install ca-certificates curl gnupg
sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg
echo   "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" |   sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

sudo groupadd -f docker
sudo usermod -aG docker $USER
newgrp docker
```

## Vagrant and VirtualBox

```bash
sudo apt install virtualbox

wget -O- https://apt.releases.hashicorp.com/gpg | sudo gpg --dearmor -o /usr/share/keyrings/hashicorp-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
sudo apt update && sudo apt install vagrant
```

## pyenv

```bash
sudo apt-get update
sudo apt-get install -y build-essential checkinstall
sudo apt-get install -y libreadline-gplv2-dev libncursesw5-dev libssl-dev libsqlite3-dev tk-dev libgdbm-dev libc6-dev libbz2-dev
sudo apt-get install -y gdb lcov libbz2-dev libffi-dev libgdbm-dev libgdbm-compat-dev liblzma-dev libncurses5-dev libreadline6-dev libsqlite3-dev libssl-dev lzma lzma-dev tk-dev uuid-dev zlib1g-dev

git clone https://github.com/pyenv/pyenv.git ~/.pyenv
cd ~/.pyenv && src/configure && make -C src && cd
```

Add to `.zprofile`:

```bash
# pyenv
export PYENV_ROOT="$HOME/.pyenv"
export PATH=$PATH:~/.pyenv/bin
export PYTHON_CONFIGURE_OPTS='--enable-optimizations --with-lto'
export PYTHON_CFLAGS='-march=native -mtune=native' 
eval "$(pyenv init -)"
```

Install desired Python versions:

```bash
pyenv install 3.11
pyenv global 3.11
```

## nvm

```bash
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/master/install.sh | bash
```

Remove added lines in `.zshrc` and add them back to `.zprofile`:

```bash
# nvm
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
```

Log out and in again or source `.zprofile`.

Install latest node LTS version:

```bash
nvm install --lts
```
