# load zgenom
if [[ ! -d ~/.zgenom ]]; then
    git clone https://github.com/jandamm/zgenom ~/.zgenom
fi

export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8
export GPG_TTY=$(tty)
export LSCOLORS="ExFxCxDxBxegedabagacad"
export LS_COLORS="di=1;34:ln=1;35:so=1;32:pi=1;33:ex=1;31:bd=34;46:cd=34;43:su=30;41:sg=30;46:tw=30;42:ow=30;43"

# general zsh options
setopt autocd autopushd

# history
setopt hist_ignore_dups hist_expire_dups_first hist_ignore_space hist_verify share_history
HISTFILE=~/.zsh_history
HISTSIZE=10000
SAVEHIST=10000

# aliases
alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias vi='nvim'
alias vim='nvim'
alias gl='git log --oneline --graph --remotes --decorate --all'
alias glop="git log --decorate --pretty=format:'%C(yellow)%h %Creset| %C(magenta)%ad %Creset| %C(cyan)%<(20)%an %Creset| %s' --date=short"
alias tmux='direnv exec / tmux -T RGB'  # use -T 256 if RGB is not supported

# load zgenom
source ~/.zgenom/zgenom.zsh
if ! zgenom saved || test ~/.zgenrc -nt $ZGEN_INIT; then
    source ~/.zgenrc
    zgenom save
fi

[[ $TMUX = "" ]] && export TERM="xterm-256color"
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
[[ ! -f ~/.p10k-segments.zsh ]] || source ~/.p10k-segments.zsh

# key binding fixes
# on Mac/iTerm2, change profile key bindings to xterm
# type "cat" and then press desired key to find its code

# home and end keys
bindkey "^[[1~" beginning-of-line
bindkey "^[[4~" end-of-line

# forward and backward by word in plain shell
bindkey "^[[1;9D" backward-word
bindkey "^[[1;9C" forward-word

# forward and backward by word in tmux shell
bindkey "^[[1;3D" backward-word
bindkey "^[[1;3C" forward-word

# shift left/right moves by chars
bindkey "^[[1;2D" backward-char
bindkey "^[[1;2C" forward-char

# shift+option left/right moves by words
bindkey "^[[1;10D" backward-word
bindkey "^[[1;10C" forward-word

# up/down history search
autoload -U up-line-or-beginning-search
autoload -U down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search
bindkey "^[OA" up-line-or-beginning-search # Up
bindkey "^[OB" down-line-or-beginning-search # Down
bindkey "^[[A" up-line-or-beginning-search # Up
bindkey "^[[B" down-line-or-beginning-search # Down

# fzf key bindings
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

# do not modify this file, add your custom settings to ~/.zprofile instead
