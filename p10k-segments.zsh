typeset -g POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(
    remote_hostname         # hostname if connected via SSH
    dir                     # current directory
    prompt_char             # prompt symbol
)

typeset -g POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(
    status                  # exit code of the last command
    command_execution_time  # duration of the last command
    background_jobs         # presence of background jobs
    virtualenv              # python virtual environment (https://docs.python.org/3/library/venv.html)
    node_version            # node.js version
    go_version              # go version (https://golang.org)
    rust_version            # rustc version (https://www.rust-lang.org)
    dotnet_version          # .NET version (https://dotnet.microsoft.com)
    php_version             # php version (https://www.php.net/)
    laravel_version         # laravel php framework version (https://laravel.com/)
    java_version            # java version (https://www.java.com/)
    vcs                     # git status
    customvpn               # VPN status (custom)
)

typeset -g POWERLEVEL9K_STATUS_OK=false
typeset -g POWERLEVEL9K_VIRTUALENV_SHOW_PYTHON_VERSION=true

function prompt_remote_hostname() {
    if [[ "$SSH_CLIENT" != "" ]]; then
        p10k segment -f 208 -t "$(hostname -s)"
    fi
}

function prompt_customvpn() {
    _S=
    if [[ -f /tmp/vtunnel.pid ]]; then
        kill -0 $(cat /tmp/vtunnel.pid) && _S=${_S}T
    fi
    if [[ -f /tmp/vproxy.pid ]]; then
        kill -0 $(cat /tmp/vproxy.pid) && _S=${_S}P
    fi
    if [[ -f /tmp/vcisco ]]; then
        _S=${_S}V
    fi
    if [[ "$_S" != "" ]]; then
        p10k segment -f 208 -t $_S
    fi
}
