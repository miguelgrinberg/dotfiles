Docker Engine Install
=====================

Requirements: VirtualBox and Vagrant
Setup:
- Run `vagrant up` to create Docker VM
- Add `192.168.66.4 docker` to your `/etc/hosts` file
- Add `export DOCKER_HOST=docker:2375` to your `.bashrc`
