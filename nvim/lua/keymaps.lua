local nmap = function(key, command, opts)
    vim.keymap.set('n', key, command, opts)
end

local imap = function(key, command, opts)
    vim.keymap.set('i', key, command, opts)
end

local vmap = function(key, command, opts)
    vim.keymap.set('v', key, command, opts)
end

-- panels
nmap('<Leader>l', '<Cmd>Lazy<CR>', { desc = 'Lazy' })
nmap('<Leader>k', '<Cmd>WhichKey<CR>', { desc = 'WhichKey' })
nmap('<Leader>m', '<Cmd>Mason<CR>', { desc = 'Mason' })
nmap('<leader>a', '<cmd>AerialToggle<CR>', { desc = 'Aerial' })
nmap('<Leader>e', '<Cmd>Neotree toggle<CR>', { desc = 'Neotree' })

-- toggles
nmap('<Leader>w', function()
    vim.cmd [[set wrap!]]
    vim.cmd [[set linebreak!]]
end, { desc = 'Word wrap' })

nmap('<Leader>d', function()
    vim.g.lsp_cmp_enabled = not vim.g.lsp_cmp_enabled
    if vim.g.lsp_cmp_enabled then
        vim.diagnostic.enable()
    else
        vim.diagnostic.disable()
    end
end, { desc = 'Toggle Diagnostics, LSP and Completions' })

nmap('<leader>s', function()
    vim.cmd [[set spell!]]
end, { desc = 'Spell checking' })

nmap('<leader>p', function()
    vim.cmd [[MarkdownPreviewToggle]]
end, { desc = 'Markdown Preview' })

-- buffers
nmap('<leader>,', '<Cmd>BufferPrevious<CR>', { desc = 'Previous buffer' })
nmap('<leader>.', '<Cmd>BufferNext<CR>', { desc = 'Next buffer' })
nmap('<leader><', '<Cmd>BufferMovePrevious<CR>', { desc = 'Move buffer left' })
nmap('<leader>>', '<Cmd>BufferMoveNext<CR>', { desc = 'Move buffer right' })
nmap('<A-1>', '<Cmd>BufferGoto 1<CR>', { desc = 'Go to buffer 1' })
nmap('<A-2>', '<Cmd>BufferGoto 2<CR>', { desc = 'Go to buffer 2' })
nmap('<A-3>', '<Cmd>BufferGoto 3<CR>', { desc = 'Go to buffer 3' })
nmap('<A-4>', '<Cmd>BufferGoto 4<CR>', { desc = 'Go to buffer 4' })
nmap('<A-5>', '<Cmd>BufferGoto 5<CR>', { desc = 'Go to buffer 5' })
nmap('<A-6>', '<Cmd>BufferGoto 6<CR>', { desc = 'Go to buffer 6' })
nmap('<A-7>', '<Cmd>BufferGoto 7<CR>', { desc = 'Go to buffer 7' })
nmap('<A-8>', '<Cmd>BufferGoto 8<CR>', { desc = 'Go to buffer 8' })
nmap('<A-9>', '<Cmd>BufferGoto 9<CR>', { desc = 'Go to buffer 9' })
nmap('<A-0>', '<Cmd>BufferLast<CR>', { desc = 'Go to previous buffer' })
nmap('<A-p>', '<Cmd>BufferPin<CR>', { desc = 'Pin/Unpin buffer' })
nmap('<A-q>', '<Cmd>BufferClose<CR>', { desc = 'Close buffer' })
nmap('<A-Q>', '<Cmd>BufferCloseAllButCurrentOrPinned<CR>', { desc = 'Close all but current or pinned' })

-- splits
nmap('<C-l>', function()
    require('focus').split_nicely()
end, { desc = 'Split Nicely' })
nmap('<leader><left>', function()
    require('focus').split_command('h')
end, { desc = 'Split Left' })
nmap('<leader><right>', function()
    require('focus').split_command('l')
end, { desc = 'Split Right' })
nmap('<leader><up>', function()
    require('focus').split_command('j')
end, { desc = 'Split Up' })
nmap('<leader><down>', function()
    require('focus').split_command('k')
end, { desc = 'Split Down' })

local ignore_filetypes = { 'neo-tree', 'aerial' }
local ignore_buftypes = { 'nofile', 'prompt', 'popup' }
local augroup = vim.api.nvim_create_augroup('FocusDisable', { clear = true })

vim.api.nvim_create_autocmd('WinEnter', {
    group = augroup,
    callback = function(_)
        if vim.tbl_contains(ignore_buftypes, vim.bo.buftype) then
            vim.b.focus_disable = true
        end
    end,
    desc = 'Disable focus autoresize for BufType',
})

vim.api.nvim_create_autocmd('FileType', {
    group = augroup,
    callback = function(_)
        if vim.tbl_contains(ignore_filetypes, vim.bo.filetype) then
            vim.b.focus_disable = true
        end
    end,
    desc = 'Disable focus autoresize for FileType',
})

-- telescope
nmap('<leader>ff', function()
    require('telescope.builtin').find_files()
end, { desc = 'Find files' })
nmap('<leader>fg', function()
    require('telescope.builtin').live_grep()
end, { desc = 'Find in files' })
nmap('<leader>fb', function()
    require('telescope.builtin').buffers()
end, { desc = 'Find buffers' })
nmap('<leader>fh', function()
    require('telescope.builtin').help_tags()
end, { desc = 'Find help' })

-- comment and uncomment lines and selections
nmap('<A-a>', function()
    require('Comment.api').toggle.linewise.current()
end, { desc = 'Comment/Uncomment line' })
nmap('<A-b>', function()
    require('Comment.api').toggle.blockwise.current()
end, { desc = 'Comment/Uncomment line' })
vmap('<A-a>', function()
    local esc = vim.api.nvim_replace_termcodes('<ESC>', true, false, true)
    vim.api.nvim_feedkeys(esc, 'nx', false)
    require('Comment.api').toggle.linewise(vim.fn.visualmode())
end, { desc = 'Comment/Uncomment selection' })
vmap('<A-b>', function()
    local esc = vim.api.nvim_replace_termcodes('<ESC>', true, false, true)
    vim.api.nvim_feedkeys(esc, 'nx', false)
    require('Comment.api').toggle.blockwise(vim.fn.visualmode())
end, { desc = 'Comment/Uncomment selection' })

-- indenting and unindenting
nmap('<Tab>', '>>', { desc = 'Indent current line' })
nmap('<S-Tab>', '<<', { desc = 'Unindent current line' })
vmap('<Tab>', '>gv', { noremap = true, desc = 'Indent selected lines' })
vmap('<S-Tab>', '<gv', { noremap = true, desc = 'Unindent selected lines' })
imap('<S-Tab>', '<Esc><<i', { desc = 'Unindent current line' })

-- clipboard
vmap('<C-x>', '"+c', { desc = 'Cut selection to clipboard' })
vmap('<C-c>', '"+y', { desc = 'Copy selection to clipboard' })
nmap('<C-v>', '"+gP', { desc = 'Paste from clipboard' })
vmap('<C-v>', '""x"+gP', { desc = 'Paste from clipboard' })
imap('<C-v>', function()
    if vim.fn.col('.') == vim.fn.col('$') then
        vim.cmd [[normal! "+gp']]
        vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes('<Right>', true, false, true), 'n', false)
    else
        vim.cmd [[normal! "+gP]]
    end
end, { silent = true, desc = 'Paste from clipboard' })

-- word movement
nmap('<A-Left>', 'b', { desc = 'Move to previous word' })
nmap('<A-Right>', 'w', { desc = 'Move to next word' })
nmap('<A-S-Left>', 'vb', { desc = 'Select previous word' })
nmap('<A-S-Right>', 'vw', { desc = 'Select next word' })
vmap('<A-S-Left>', 'b', { desc = 'Move to previous word' })
vmap('<A-S-Right>', 'w', { desc = 'Move to next word' })
vmap('<A-S-Up>', 'k', { desc = 'Move to previous line' })
vmap('<A-S-Down>', 'j', { desc = 'Move to next line' })
imap('<A-S-Left>', '<Esc>vb', { desc = 'Move to previous word' })
imap('<A-S-Right>', '<Esc><Right>vw', { desc = 'Move to next word' })

-- debugger
nmap('<F5>', function()
    require('dap').continue()
end, { desc = 'Debug' })
nmap('<F17>', function()
    require('dap').terminate()
end, { desc = 'End debugger' })  -- Shift F5
nmap('<F10>', function()
    require('dap').step_over()
end, { desc = 'Step over' })
nmap('<F11>', function()
    require('dap').step_into()
end, { desc = 'Step into' })
nmap('<F23>', function()
    require('dap').step_out()
end, { desc = 'Step out' })  -- Shift F11
nmap('<leader>b', function()
    require('dap').toggle_breakpoint()
end, { desc = 'Toggle breakpoint' })
nmap('<leader>B', function()
    require('dap').set_breakpoint(vim.fn.input('Breakpoint condition: '))
end, { desc = 'Set conditional breakpoint' })
nmap('<leader>L', function()
    require('dap').set_breakpoint(nil, nil, vim.fn.input('Logpoint message: '))
end, { desc = 'Set logpoint breakpoint' })
nmap('<C-k>', function()
    require("dapui").eval()
end, { desc = 'Evaluate expression' })
vmap('<C-k>', function()
    require("dapui").eval()
end, { desc = 'Evaluate expression' })

-- trouble
nmap('<leader>xx', function() require('trouble').toggle("diagnostics") end, { desc = 'Trouble' })
nmap('<leader>xw', function() require('trouble').open('workspace_diagnostics') end, { desc = 'Trouble Workspace' })
nmap('<leader>xd', function() require('trouble').open('document_diagnostics') end, { desc = 'Trouble Document' })
nmap('<leader>xq', function() require('trouble').open('quickfix') end, { desc = 'Trouble Quickfix' })
nmap('<leader>xl', function() require('trouble').open('loclist') end, { desc = 'Trouble Location List' })
nmap('<leader>xt', function() require('trouble').open('telescope') end, { desc = 'Trouble Telescope' })
nmap('gR', function() require('trouble').open('lsp_references') end, { desc = 'Trouble LSP References' })

-- diagnostics
nmap('[d', vim.diagnostic.goto_prev, { desc = 'Go to previous diagnostic message' })
nmap(']d', vim.diagnostic.goto_next, { desc = 'Go to next diagnostic message' })
