local M = {}

function M.setup()
    local lsp = require('lsp-zero').preset({})

    lsp.on_attach(function(client, bufnr)
        -- see :help lsp-zero-keybindings
        -- to learn the available actions
        lsp.default_keymaps({
            buffer = bufnr,
            preserve_mappings = false,
        })
    end)

    lsp.set_sign_icons({
        error = '', -- '✘',
        warn = '', -- '▲',
        hint = '', -- '⚑',
        info = '', -- '»',
    })

    require('neodev').setup()
    require('mason').setup()
    require('mason-lspconfig').setup()
    require('mason-lspconfig').setup_handlers({
        function(server_name)
            require('lspconfig')[server_name].setup({})
        end,

        ['lua_ls'] = function()
            require('lspconfig').lua_ls.setup({
                settings = {
                    Lua = {
                        workspace = {
                            checkThirdParty = false,
                        }
                    },
                },
            })
        end,
    })
    lsp.setup()
end

return M
