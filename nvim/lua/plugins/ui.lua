return {
    -- which-key
    {
        'folke/which-key.nvim',
        event = 'VeryLazy',
        opts = {},
    },

    -- onedark (color theme)
    {
        'navarasu/onedark.nvim',
        lazy = false,
        priority = 1000,
        opts = {
            style = 'warm'
        },
        init = function()
            vim.cmd [[colorscheme onedark]]
            vim.cmd [[highlight clear SpellBad]]
            vim.cmd [[highlight SpellBad gui=undercurl guisp=#ff7080]]
        end,
    },

    -- neotree
    {
        'nvim-neo-tree/neo-tree.nvim',
        cmd = 'Neotree',
        branch = 'v3.x',
        dependencies = {
            'nvim-lua/plenary.nvim',
            'nvim-tree/nvim-web-devicons',
            'MunifTanjim/nui.nvim',
        },
        opts = {
            close_if_last_window = true,
        },
    },

    -- gitsigns
    {
        'lewis6991/gitsigns.nvim',
        opts = {
            signs = {
                add = { text = '|' },
                change = { text = '|' },
                delete = { text = '_' },
                topdelete = { text = '‾' },
                changedelete = { text = '~' },
                untracked = { text = '┆' },
            },
            signcolumn = true,
        },
    },

    -- barbar (tabline)
    {
        'romgrk/barbar.nvim',
        dependencies = {
            'nvim-tree/nvim-web-devicons',
            'lewis6991/gitsigns.nvim',
        },
        config = function()
            require('barbar').setup({
                auto_hide = true,
                sidebar_filetypes = {
                    ['neo-tree'] = { event = 'BufWipeout', text = 'NeoTree' },
                },
                icons = {
                    button = '✕',
                    pinned = { button = '', filename = true },
                    gitsigns = {
                        added = { enabled = true, icon = '+' },
                        changed = { enabled = true, icon = '~' },
                        deleted = { enabled = true, icon = '-' },
                    },
                },
            })

            -- hack: address conflict between barbar and the onedark theme
            vim.api.nvim_set_hl(0, 'BufferCurrentADDED', { bg = '#2c2d30', fg = '#7ea662' })
            vim.api.nvim_set_hl(0, 'BufferCurrentCHANGED', { bg = '#2c2d30', fg = '#4fa6ed' })
            vim.api.nvim_set_hl(0, 'BufferCurrentDELETED', { bg = '#2c2d30', fg = '#e55561' })
        end,
    },

    -- lualine
    {
        'nvim-lualine/lualine.nvim',
        opts = {
            options = {
                theme = 'onedark',
            },
        },
    },

    -- indent_blankline (indent guides)
    {
        'lukas-reineke/indent-blankline.nvim',
        main = 'ibl',
        event = 'VeryLazy',
        config = function()
            require('ibl').setup({
                scope = { show_start = false, show_end = false, highlight = { 'Delimiter' } },
            })
        end,
    },

    -- nvim-ufo (folding)
    {
        'kevinhwang91/nvim-ufo',
        dependencies = {
            'kevinhwang91/promise-async',
            {
                'luukvbaal/statuscol.nvim',
                config = function()
                    local builtin = require("statuscol.builtin")
                    require('statuscol').setup({
                        relculright = true,
                        segments = {
                            { text = { '%s' }, click = 'v:lua.ScSa' },
                            { text = { builtin.foldfunc }, click = 'v:lua.ScFa' },
                            { text = { builtin.lnumfunc, ' ' }, click = 'v:lua.ScLa' },
                        },
                    })
                end,
            },
        },
        event = 'BufReadPost',
        config = function()
            require('ufo').setup({
                provider_selector = function(bufnr, filetype, buftype)
                    return { 'treesitter', 'indent' }
                end,
                fold_virt_text_handler = function(virtText, lnum, endLnum, width, truncate)
                    local newVirtText = {}
                    local suffix = (' ⋯  %d'):format(endLnum - lnum)
                    local sufWidth = vim.fn.strdisplaywidth(suffix)
                    local targetWidth = width - sufWidth
                    local curWidth = 0
                    for _, chunk in ipairs(virtText) do
                        local chunkText = chunk[1]
                        local chunkWidth = vim.fn.strdisplaywidth(chunkText)
                        if targetWidth > curWidth + chunkWidth then
                            table.insert(newVirtText, chunk)
                        else
                            chunkText = truncate(chunkText, targetWidth - curWidth)
                            local hlGroup = chunk[2]
                            table.insert(newVirtText, {chunkText, hlGroup})
                            chunkWidth = vim.fn.strdisplaywidth(chunkText)
                            -- str width returned from truncate() may be less than 2nd argument, need padding
                            if curWidth + chunkWidth < targetWidth then
                                suffix = suffix .. (' '):rep(targetWidth - curWidth - chunkWidth)
                            end
                            break
                        end
                        curWidth = curWidth + chunkWidth
                    end
                    table.insert(newVirtText, {suffix, 'MoreMsg'})
                    return newVirtText
                end,
            })
            vim.cmd [[highlight FoldColumn guibg=nocombine]]
        end,
    },

    -- nvim-cursorword (highlight cursor word)
    {
        'xiyaowong/nvim-cursorword',
        event = 'VeryLazy',
        config = function()
            vim.cmd [[highlight default link CursorWord Visual]]
        end,
    },

    {
        'nvim-focus/focus.nvim',
        opts = {},
    },

    -- comment.nvim
    'JoosepAlviste/nvim-ts-context-commentstring',
    {
        'numToStr/Comment.nvim',
        config = function()
            require('Comment').setup({
                pre_hook = require('ts_context_commentstring.integrations.comment_nvim').create_pre_hook(),
            })
        end,
    },

    -- noice (notifications)
    {
        'folke/noice.nvim',
        event = 'VeryLazy',
        dependencies = {
            'MunifTanjim/nui.nvim',
        },
        opts = {
            cmdline = {
                enabled = true,
                view = 'cmdline',
                format = {
                    conceal = false,
                },
            },
            lsp = {
                override = {
                    ["vim.lsp.util.convert_input_to_markdown_lines"] = true,
                    ["vim.lsp.util.stylize_markdown"] = true,
                    ["cmp.entry.get_documentation"] = true,
                },
            },
            presets = {
                bottom_search = true,
                long_message_to_split = true,
                lsp_doc_border = true,
            },
        },
    },

    -- trouble (diagnostics, search results, etc.)
    {
        'folke/trouble.nvim',
        event = 'VeryLazy',
        dependencies = {
            'nvim-tree/nvim-web-devicons',
        },
        opts = {
        },
    },

    -- aerial (tagbar)
    {
        'stevearc/aerial.nvim',
        cmd = {'AerialOpen', 'AerialClose', 'AerialToggle', 'AerialNext', 'AerialPrev', 'AerialNextUp', 'AerialPrevDown'},
        opts = {
            on_attach = function(bufnr)
                vim.keymap.set('n', '{', '<cmd>AerialPrev<CR>', { buffer = bufnr })
                vim.keymap.set('n', '}', '<cmd>AerialNext<CR>', { buffer = bufnr })
            end,
        },
        dependencies = {
           "nvim-treesitter/nvim-treesitter",
           "nvim-tree/nvim-web-devicons",
        },
    },
}
