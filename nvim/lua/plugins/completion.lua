return {
    -- treesitter
    {
        'nvim-treesitter/nvim-treesitter',
        build = ":TSUpdate",
        config = function()
            require('nvim-treesitter.configs').setup({
                highlight = {
                    enable = true,
                },
                indent = {
                    enable = true,
                },
                ensure_installed = { 'python', 'javascript', 'html', 'css', 'lua', 'vim' },
                auto_install = true,
            })
        end,
    },

    -- luasnip (snippets)
    {
        'L3MON4D3/LuaSnip',
        event = 'InsertEnter',
        dependencies = {
            'rafamadriz/friendly-snippets',
        },
        config = function()
            require('luasnip.loaders.from_vscode').lazy_load()
            require('luasnip').config.setup({})
        end,
    },

    -- various completion plugins
    {
        'hrsh7th/nvim-cmp',
        event = 'InsertEnter',
    },
    {
        'hrsh7th/cmp-nvim-lsp',
        event = 'InsertEnter',
    },
    {
        'hrsh7th/cmp-buffer',
        event = 'InsertEnter',
    },
    {
        'hrsh7th/cmp-path',
        event = 'InsertEnter',
    },
    {
        'hrsh7th/cmp-cmdline',
        event = 'InsertEnter',
    },
    {
        'saadparwaiz1/cmp_luasnip',
        event = 'InsertEnter',
    },
    {
        'onsails/lspkind.nvim',
        event = 'InsertEnter',
    },
    {
        'zbirenbaum/copilot-cmp',
        event = 'InsertEnter',
        opts = {},
    },
    {
        'zbirenbaum/copilot.lua',
        event = 'InsertEnter',
        cmd = 'Copilot',
        opts = {
            suggestion = { enabled = false },
            panel = { enabled = false },
            filetypes = {
                markdown = true,
            },
        }
    },
}
