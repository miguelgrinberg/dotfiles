return {
    {
        'folke/lsp-colors.nvim',
        opts = {},
    },
    {
        'VonHeikemen/lsp-zero.nvim',
        branch = 'v2.x',
    },
    'neovim/nvim-lspconfig',
    {
        'williamboman/mason.nvim',
        build = function()
            pcall(vim.api.nvim_command, 'MasonUpdate')
        end,
    },
    'williamboman/mason-lspconfig.nvim',
    'folke/neodev.nvim',
}
