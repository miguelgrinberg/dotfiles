-- basics
vim.g.mapleader = ' '
vim.g.maplocalleader = ' '

-- line numbers
vim.opt.number = true

-- tabs
vim.opt.expandtab = true
vim.opt.tabstop = 4
vim.opt.shiftwidth = 4
vim.opt.softtabstop = 4
vim.opt.smartindent = true
vim.opt.colorcolumn = '80'
vim.opt.breakindent = true
-- exceptions by file type
vim.cmd [[filetype plugin on]]
vim.cmd [[autocmd FileType html setlocal tabstop=2 shiftwidth=2 softtabstop=2]]
vim.cmd [[autocmd FileType css setlocal tabstop=2 shiftwidth=2 softtabstop=2]]
vim.cmd [[autocmd FileType javascript setlocal tabstop=2 shiftwidth=2 softtabstop=2]]
vim.cmd [[autocmd FileType javascriptreact setlocal tabstop=2 shiftwidth=2 softtabstop=2]]
vim.cmd [[autocmd FileType javascript.jsx setlocal tabstop=2 shiftwidth=2 softtabstop=2]]
vim.cmd [[autocmd FileType typescript setlocal tabstop=2 shiftwidth=2 softtabstop=2]]
vim.cmd [[autocmd FileType typescriptreact setlocal tabstop=2 shiftwidth=2 softtabstop=2]]
vim.cmd [[autocmd FileType typescript.jsx setlocal tabstop=2 shiftwidth=2 softtabstop=2]]

-- folding
vim.opt.foldcolumn = '1'
vim.opt.foldlevel = 99
vim.opt.foldlevelstart = 99
vim.opt.foldenable = true
vim.opt.fillchars = { eob = ' ', fold = ' ', foldopen = '', foldsep = ' ', foldclose= '❯' }

-- search
vim.opt.ignorecase = true
vim.opt.smartcase = true
vim.opt.showmatch = true

-- text selection
vim.opt.selectmode = { 'mouse', 'key', 'cmd' }
vim.opt.mousemodel = 'popup'
vim.opt.keymodel = { 'startsel', 'stopsel' }
vim.opt.selection = 'exclusive'

-- misc options
vim.opt.termguicolors = true
vim.opt.updatetime = 250
vim.opt.timeout = true
vim.opt.timeoutlen = 300
vim.opt.wrap = false
vim.opt.spelllang = 'en_us'
vim.cmd [[autocmd FileType markdown setlocal wrap linebreak]]

-- global variables
vim.g.lsp_cmp_enabled = true
