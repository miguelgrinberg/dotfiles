#!/bin/sh

set -e
PROGNAME=$(basename $0)

die() {
    echo "$PROGNAME: $*" >&2
    exit 1
}

usage() {
    if [ "$*" != "" ] ; then
        echo "Error: $*"
    fi
    cat << EOF
Usage: $PROGNAME [OPTION ...] [foo] [bar]

Program description.

Options:
-h, --help          display this usage message and exit
-d, --delete        delete things
-o, --output [FILE] write output to file
EOF
    exit 1
}

FOO=""
BAR=""
DELETE=
OUTPUT="-"

while [ $# -gt 0 ] ; do
    case "$1" in
    -h|--help)
        usage
        ;;
    -d|--delete)
        DELETE=1
        ;;
    -o|--output)
        OUTPUT="$2"
        shift
        ;;
    -*)
        usage "Unknown option '$1'"
        ;;
    *)
        if [ -z "$FOO" ] ; then
            FOO="$1"
        elif [ -z "$BAR" ] ; then
            BAR="$1"
        else
            usage "Too many arguments"
        fi
        ;;
    esac
    shift
done

if [ -z "$BAR" ] ; then
    usage "Not enough arguments"
fi

cat <<EOF
foo=$FOO
bar=$BAR
delete=$DELETE
output=$OUTPUT
EOF
