#!/bin/bash
WHO=Miguel
GROUP_IDS="sg-a8253cd0 sg-05f2bde5b8be195bf"
PORTS="22 443"
MY_IP=$(curl https://canihazip.com/s 2>/dev/null)

function authorize_ip () {
    aws ec2 authorize-security-group-ingress --group-id $1 --ip-permissions "IpProtocol=tcp,FromPort=$2,ToPort=$2,IpRanges=[{CidrIp=$3,Description='$WHO Home'}]" 2>.temp_output
    if [[ "$?" == "0" ]]; then
        echo Authorized $3 for port $2 in $1.
    else
        grep "InvalidPermission\.Duplicate" .temp_output >/dev/null
        if [[ "$?" != "0" ]]; then
            cat .temp_output
        fi
    fi
    rm .temp_output
}

function revoke_ip () {
    aws ec2 revoke-security-group-ingress --group-id $1 --ip-permissions "IpProtocol=tcp,FromPort=$2,ToPort=$2,IpRanges=[{CidrIp=$3}]"
    echo Revoked $3 for port $2 in $1.
}

for group_id in $GROUP_IDS; do
    aws ec2 describe-security-groups --group-ids $group_id | jq -c ".SecurityGroups[0].IpPermissions[]" | while read -r port_config; do
        port=$(echo $port_config | jq -c ".FromPort")
        if [[ " $PORTS " =~ .*\ $port\ .* ]]; then
            echo $port_config | jq -c ".IpRanges[]" | while read -r ip_rule; do
                old_cidr=$(echo $ip_rule | jq -r ".CidrIp")
                description=$(echo $ip_rule | jq -r ".Description")
                if [[ "$description" == "$WHO Home" ]]; then
                    if [[ "$old_cidr" == "$MY_IP/32" ]]; then
                        echo $old_cidr is already set for port $port.
                    else
                        revoke_ip $group_id $port $old_cidr
                    fi
                fi
            done
        fi
    done

    for port in $PORTS; do
        authorize_ip $group_id $port "$MY_IP/32"
    done
done
