# homebrew
# eval "$(/opt/homebrew/bin/brew shellenv)"

# direnv
# eval "$(direnv hook zsh)"

# pyenv
# export PYENV_ROOT="$HOME/.pyenv"
# export PATH=$PATH:~/.pyenv/bin
# export PYTHON_CONFIGURE_OPTS='--enable-optimizations --with-lto'
# export PYTHON_CFLAGS='-march=native -mtune=native' 
# eval "$(pyenv init -)"

# nvm
# export NVM_DIR="$HOME/.nvm"
# [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
# [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
